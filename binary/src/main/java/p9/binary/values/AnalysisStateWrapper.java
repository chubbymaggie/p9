package p9.binary.values;

import bindead.debug.XmlToHtml;
import bindead.domainnetwork.interfaces.RootDomain;

/**
 * A class to wrap and cache an analysis state, a so called domain value.
 * It is also meant to cache its various possible representations,
 * e.g. the ways it can be rendered, printed.
 *
 * TODO: implement all the methods returning strings as lazy cached.
 *
 * @author Bogdan Mihaila
 */
public class AnalysisStateWrapper {
  private final RootDomain<?> domain;

  public AnalysisStateWrapper (RootDomain<?> domain) {
    this.domain = domain;
  }

  public RootDomain<?> getState () {
    return domain;
  }

  public String toTextDump () {
    return getState().toString();
  }

  public String toCompactTextDump () {
    StringBuilder builder = new StringBuilder();
    getState().toCompactString(builder);
    return builder.toString();
  }

  public String toXMLDump () {
    return getState().toXml();
  }

  public String toHTMLDump () {
    String htmlOutput = XmlToHtml.renderAsHtml(toXMLDump());
    return htmlOutput;
  }

  public String toHTMLDumpFiltered (String regexToShow) {
    // TODO: see how to integrate the regex filter
    return toHTMLDump();
  }

}
