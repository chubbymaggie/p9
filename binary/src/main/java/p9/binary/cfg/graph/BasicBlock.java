package p9.binary.cfg.graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javalx.mutablecollections.CollectionHelpers;
import rreil.disassembler.Instruction;

/**
 * A collection of instructions, with the property that except for the first and last
 * instructions in the block each instruction has exactly one predecessor and one successor.
 *
 * @author Bogdan Mihaila
 */
public class BasicBlock implements Iterable<Instruction> {
  private final List<Instruction> instructions = new ArrayList<>();

  public BasicBlock () {
  }

  public BasicBlock (Instruction instruction) {
    add(instruction);
  }

  public BasicBlock (Collection<Instruction> instructions) {
    addAll(instructions);
  }

  private void add (Instruction instruction) {
    instructions.add(instruction);
  }

  private void addAll (Collection<Instruction> instructions) {
    this.instructions.addAll(instructions);
  }

  public void addAll (BasicBlock other) {
    this.instructions.addAll(other.instructions);
  }

  @Override public Iterator<Instruction> iterator () {
    return instructions.iterator();
  }

  /**
   * Return a view on this basic block that iterates the instructions in reversed order,
   * that is, from last instruction to first.
   */
  public Iterable<Instruction> reversedOrderView () {
    return CollectionHelpers.reversedIterable(instructions);
  }

  public int size () {
    return instructions.size();
  }

  public Instruction get (int line) {
    return instructions.get(line);
  }

  @Override public String toString () {
    StringBuilder builder = new StringBuilder();
    Iterator<Instruction> it = instructions.iterator();
    while (it.hasNext()) {
      Instruction instruction = it.next();
      builder.append(instruction.address());
      builder.append(": ");
      builder.append(instruction);
      if (it.hasNext())
        builder.append("\n");
    }
    return builder.toString();
  }
}
