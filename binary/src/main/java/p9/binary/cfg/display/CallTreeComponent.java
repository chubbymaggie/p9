package p9.binary.cfg.display;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;

import p9.binary.WindowsUtils;
import p9.binary.cfg.controller.events.CallTreeViewEventBus;
import p9.binary.cfg.controller.events.CallTreeViewEventBus.CallTreeFocusContextEvent;
import p9.binary.cfg.controller.events.CallTreeViewEventBus.CallTreeViewEventListener;
import p9.binary.cfg.graph.AnalysisResult;
import p9.binary.cfg.graph.CallTree;
import prefuse.Visualization;
import prefuse.controls.Control;
import prefuse.controls.ControlAdapter;
import prefuse.util.FontLib;
import prefuse.util.ui.JFastLabel;
import prefuse.util.ui.JSearchPanel;
import prefuse.visual.VisualItem;
import bindead.analyses.algorithms.data.CallString;

@TopComponent.Description(preferredID = "CallTreeComponent", iconBase = CallTreeComponent.iconPath,
    persistenceType = TopComponent.PERSISTENCE_NEVER)
@TopComponent.Registration(mode = "reconstruction", openAtStartup = false)
@Messages({
  "CTL_CallTreeComponentAction=Open Call Graph",
  "HINT_CallTreeComponentAction=Display the Call Graph for the binary.",
  "# parameter {0} is the file name",
  "CTL_CallTreeComponentTitle=Binary: {0}",
  "HINT_CallTreeComponent=Call Graph for the binary file: {0}."
})
public class CallTreeComponent extends TopComponent {
  private static final long serialVersionUID = 1L;
  protected static final String iconPath = "p9/binary/icons/binary-document-16x16.png";
  private static final ImageIcon icon = ImageUtilities.loadImageIcon(iconPath, false);
  private final CallTreeView callTreeView;
  private CallTreeViewEventListener listener;

  public CallTreeComponent (CallTreeView callTree, String eventsID) {
    this.callTreeView = callTree;
    // create a search panel for the tree
    JSearchPanel search = new JSearchPanel(
      callTree.getVisualization(),
      CallTreeView.$Nodes,
      Visualization.SEARCH_ITEMS,
      CallTree.$Label, true, true);
    search.setShowResultCount(true);
    search.setBorder(BorderFactory.createEmptyBorder(5, 5, 4, 0));
    search.setFont(FontLib.getFont("Tahoma", Font.PLAIN, 11));
    search.setBackground(ColorConfig.$Background);
    search.setForeground(ColorConfig.$Foreground);

    final JFastLabel title = new JFastLabel("                 ");
    title.setPreferredSize(new Dimension(350, 20));
    title.setVerticalAlignment(SwingConstants.BOTTOM);
    title.setBorder(BorderFactory.createEmptyBorder(3, 0, 0, 0));
    title.setFont(FontLib.getFont("Tahoma", Font.PLAIN, 12));
    title.setBackground(ColorConfig.$Background);
    title.setForeground(ColorConfig.$Foreground);

    callTree.addControlListener(
        new ControlAdapter() {
          @Override public void itemEntered (VisualItem item, MouseEvent e) {
            if (item.canGetString(CallTree.$Label)) {
              CallString callString = (CallString) item.get(CallTree.$Callsite);
              if (callString != null)
                title.setText(callString.toString());
            }
          }

          @Override public void itemExited (VisualItem item, MouseEvent e) {
            title.setText(null);
          }
        });

    Box box = new Box(BoxLayout.X_AXIS);
    box.add(Box.createHorizontalStrut(10));
    box.add(title);
    box.add(Box.createHorizontalGlue());
    box.add(search);
    box.add(Box.createHorizontalStrut(3));
    box.setBackground(ColorConfig.$Background);

    JPanel body = new JPanel(new BorderLayout());
    body.add(callTree, BorderLayout.CENTER);
    body.add(box, BorderLayout.SOUTH);
    body.setBackground(ColorConfig.$Background);
    body.setForeground(ColorConfig.$Foreground);

    setLayout(new BorderLayout());
    add(body, BorderLayout.CENTER);
    WindowsUtils.dockInMode("reconstruction", this);
    registerEventsListener(eventsID);
  }

  @Override protected void componentClosed () {
    CallTreeViewEventBus.getInstance().removeListener(listener);
  }

  private void registerEventsListener (final String interestedID) {
    listener = new CallTreeViewEventListener() {
      @Override public void notify (CallTreeFocusContextEvent event) {
        if (!event.isConsumed() && event.getReceiverID().equals(interestedID)) {
          CallTreeComponent.this.requestVisible();
          callTreeView.highlightFunction(event.getCallstring());
          event.consume();
        }
      }
    };
    CallTreeViewEventBus.getInstance().addListener(listener);
  }

  public void addControlListener (Control control) {
    callTreeView.addControlListener(control);
  }

  public static Action getOpenAction (final AnalysisResult analysis, final String fileName) {
    Action action = new AbstractAction() {
      @Override public void actionPerformed (ActionEvent e) {
        String eventsID = CallTreeViewEventBus.getEventsID(analysis);
        CallTreeComponent display = new CallTreeComponent(new CallTreeView(new CallTree(analysis)), eventsID);
        display.setDisplayName(Bundle.CTL_CallTreeComponentTitle(fileName));
        display.setDisplayName("Binary: " + fileName);
        display.setToolTipText(Bundle.HINT_CallTreeComponent(fileName));
        display.open();
        display.requestActive();
      }
    };
    action.putValue(Action.SMALL_ICON, icon);
    action.putValue(Action.NAME, Bundle.CTL_CallTreeComponentAction());
    action.putValue(Action.SHORT_DESCRIPTION, Bundle.HINT_CallTreeComponentAction());
    return action;
  }

}
