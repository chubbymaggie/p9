package p9.binary.cfg.display.items.highlight;

/**
 * This class contains the information, what data has to be highlighted by the
 * items.
 *
 * @author Raphael Dümig
 */
public class ObjectHighlight implements DataHighlight<Object> {
  
  Object dataToHighlight;

  public ObjectHighlight(Object dataToHighlight) {
    this.dataToHighlight = dataToHighlight;
  }
  
  @Override
  public boolean contains(Object data) {
    return dataToHighlight.equals(data);
  }
}
