package p9.binary.cfg.display.items;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.WeakHashMap;

/**
 *
 * @author Raphael Dümig <duemig@in.tum.de>
 */
public class TableLayout implements LayoutManager {

  private LinkedList<Double> columnWidths;
  private double lineWidth;
  
  protected List<DrawableItem> items;
  protected Map<DrawableItem, Point2D> posMap;
  private ItemSize size;

  public TableLayout() {
    items = new LinkedList<>();
    columnWidths = new LinkedList<>();
    posMap = new WeakHashMap<>();
    size = null;

    lineWidth = 0;
  }

  /**
   * append item to the graphical block
   *
   * @param item the item to be appended
   */
  @Override
  public void addItem(DrawableItem item) {
    items.add(item);
    updateLayout();
  }

  /**
   * append a list of items to the graphical block
   *
   * @param items list of items to be appended
   */
  @Override
  public void addAllItems(Iterable<DrawableItem> newItems) {
    for(DrawableItem di : newItems) {
      items.add(di);
    }
    updateLayout();
  }

  /**
   * insert an item in the graphical block
   *
   * @param item the item to be inserted
   * @param index the index where the item is inserted
   */
  @Override
  public void insertItem(DrawableItem item, int index) {
    items.add(index, item);
    updateLayout();
  }

  public void insertBeforeItem(DrawableItem insertItem, DrawableItem locationItem) {
    insertItem(insertItem, getIndex(locationItem));
  }

  /**
   * remove the item at index #index in the graphical block
   *
   * @param index
   */
  @Override
  public void removeItem(int index) {
    items.remove(index);
    updateLayout();
  }

  /**
   * remove all items from the layout
   */
  @Override
  public void removeAllItems() {
    items = new LinkedList<>();
    updateLayout();
  }
  
  /**
   * Recalculate the widths of the columns in the layout.
   * 
   * Columns are the addresses, opcodes (if shown), mnemonics and operands.
   */
  private void updateMaxWidths() {

    columnWidths = new LinkedList<>();
    lineWidth = 0.0;
    
    for(DrawableItem line: this) {
      int columnNumber = 0;
      
      for(DrawableItem column : line) {
        double columnSize;
        try {
          columnSize = column.getItemWidth();
        } catch(NullPointerException e) {
          columnSize = 0.0;
        }
        
        if (columnNumber < columnWidths.size()) {
          if (columnSize > columnWidths.get(columnNumber)) {
            columnWidths.set(columnNumber, columnSize);
          }
        } else {
          columnWidths.add(columnSize);
        }
        
        columnNumber++;
      }
      
      if (line.getItemWidth() > lineWidth) {
        lineWidth = line.getItemWidth();
      }
    }
    
    double sum = 0.0;
    for(Double columnWidth : columnWidths) {
      sum += columnWidth;
    }
    if (sum > lineWidth) {
      lineWidth = sum;
    }
  }

  /**
   * Update the positions of all items in this layout.
   */
  @Override
  public void updateLayout() {
    // update the column sizes before applying new sizes to the elements
    updateMaxWidths();
    
    size = new ItemSize(0, 0);

    if(items == null) {
        System.err.println("ERROR: no item list!");
        return;
    }

    for (DrawableItem line : items) {
      if (line == null) {
        continue;
      }
      
      boolean updateLineLayout = false;
      int columnNumber = 0;
      
      // alignment of instructions
      for (DrawableItem column : line) {
        if (columnWidths.get(columnNumber) > column.getItemWidth()) {
          column.setItemSize(new ItemSize(
                  columnWidths.get(columnNumber),
                  column.getItemHeight())
          );
          updateLineLayout = true;
        }
        
        if (updateLineLayout) {
          line.update();
        }
        
        columnNumber++;
      }
      
      line.setItemSize(new ItemSize(lineWidth, line.getItemHeight()));

      // now that we have aligned all the inner items, update the size of the BasicBlockItem
      ItemSize lineSize = line.getItemSize();

      double nextYPos = size.getHeight() + lineSize.getHeight();
      posMap.put(line, new Point2D.Double(0, size.getHeight()));

      double width;
      if (lineSize.getWidth() > size.getWidth()) {
        width = lineSize.getWidth();
      } else {
        width = size.getWidth();
      }

      size.setSize(width, nextYPos);
    }
  }

  @Override
  public Point2D getPosition(DrawableItem item) throws NoSuchElementException {
    Point2D pos = posMap.get(item);
    if(pos == null)
      throw new NoSuchElementException();
    return pos;
  }

  @Override
  public ItemSize getLayoutSize() {
    updateLayout();
    return size;
  }

  @Override
  public void setLayoutSize(ItemSize size) {
    if (size.isLargerThan(this.size)) {
      this.size = (ItemSize) size.clone();
    }
  }

  @Override
  public Iterator<DrawableItem> iterator() {
    return items.iterator();
  }

  // return the item at coordinates #position in the layout
  @Override
  public DrawableItem getItemAt(Point2D position) {
    for(DrawableItem i: items) {
      Rectangle2D itemBox = new Rectangle2D.Double();
      itemBox.setFrame(posMap.get(i), i.getItemSize());

      if(itemBox.contains(position))
        return i;
    }

    return null;
  }

  /**
   * Obtain the item at the given line (line count starting at 1!).
   *
   * @param lineNumber the line number of the item you want to get
   * @return the item or null if there is no such line
   */
  public DrawableItem getItemInLine(int lineNumber) {
    for(DrawableItem i: items) {
      if(lineNumber == 0)
        return i;

      lineNumber--;
    }

    return null;
  }

  @Override
  public Point2D getRelativePos(Point2D position, DrawableItem item) throws NoSuchElementException {
    Point2D itemPos = posMap.get(item);
    if(itemPos == null) {
      throw new NoSuchElementException("The element is not contained in the BasicBlockLayout!");
    }

    return new Point2D.Double(position.getX() - itemPos.getX(),
                              position.getY() - itemPos.getY());
  }

  @Override
  public int getIndex(DrawableItem item) {
    return items.indexOf(item);
  }

  @Override
  public int numberOfElements() {
    return items.size();
  }
}
