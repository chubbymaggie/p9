package p9.binary.cfg.display.items;

import java.awt.font.TextAttribute;
import java.text.AttributedString;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Raphael Dümig
 */
public abstract class AttributedTextItem extends TextItem {

  private final List<AttributeRange> attributes;

  public AttributedTextItem(String text) {
    super(text);
    this.attributes = new LinkedList<>();
  }

  public void addAttribute(AttributeRange ar) {
    attributes.add(ar);
  }

  @Override
  protected List<AttributedString> getAttributedStrings() {
    List<AttributedString> result = super.getAttributedStrings();

    for (AttributeRange ar : attributes) {
      ar.applyTo(result, getString());
    }

    return result;
  }
  
  // add an attribute to a list of AttributedStrings that are interpreted as
  // separated lines of a String
  // the whole String has also to be given in its unattributed representation (#originalString)
  public static void addAttribute(List<AttributedString> attributedLines, String originalString,
          TextAttribute attribute, Object attributeValue, int start, int end) {
    int lineOffset = 0;
    boolean active = false;

    Iterator<AttributedString> attrLineIter = attributedLines.iterator();
    AttributedString attrLine;
    int lineStart = 0;

    for (String line : originalString.split("\n")) {
      attrLine = attrLineIter.next();

      if (!active) {
        if (start < lineOffset + line.length()) {
          lineStart = start - lineOffset;
          active = true;
        }
      }
      if (active) {
        // end in this line
        if (end <= lineOffset + line.length()) {
          attrLine.addAttribute(attribute, attributeValue, lineStart, end - lineOffset);
          // finished! we can stop iterating here!
          break;
        } // end in one of the following lines
        else {
          attrLine.addAttribute(attribute, attributeValue, lineStart, line.length());
        }

        // in the next line, the range already starts at 0
        lineStart = 0;
      }

      // add the number of characters in this line plus the newline character
      lineOffset += line.length() + 1;
    }
  }


  public class AttributeRange {

    TextAttribute attribute;
    Object attributeValue;
    int start, end;
    
    public AttributeRange(TextAttribute attribute, Object attributeValue,
            int start, int end) {
      this.attribute = attribute;
      this.attributeValue = attributeValue;
      this.start = start;
      this.end = end;
    }

    public void applyTo(AttributedString attrStr) {
      attrStr.addAttribute(attribute, attributeValue, start, end);
    }
    
    public void applyTo(List<AttributedString> attributedLines, String originalString) {
      addAttribute(attributedLines, originalString, attribute, attributeValue, start, end);
    }
    
  }
}
