
package p9.binary.cfg.display.items.instructions.operands.rreil;

import p9.binary.cfg.display.items.ContainerItem;
import p9.binary.cfg.display.items.ListLayout;
import p9.binary.cfg.display.items.TextItem;
import rreil.lang.Rhs;
import static p9.binary.cfg.display.items.instructions.operands.rreil.RReilOperandItem.toItem;

/**
 *
 * @author Raphael Dümig
 */
public class RReilAddressOperandItem extends ContainerItem {
  
  private static final TextItem start = new TextItem("[");
  private static final TextItem end = new TextItem("]");
  
  static {
    start.setPadding(PADDING_LEFT, 1);
    start.setPadding(PADDING_RIGHT, 5);
    end.setPadding(PADDING_LEFT, 5);
    end.setPadding(PADDING_RIGHT, 1);
  }
  
  public RReilAddressOperandItem(Rhs address, boolean numbersInHex) {
    super( new ListLayout(null, start, end) );
    
    addItem(toItem(address, numbersInHex));
  }
}
