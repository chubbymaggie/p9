package prefuse.controls;

import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import prefuse.Display;
import prefuse.render.HighlightableLineLabelRenderer;
import prefuse.visual.NodeItem;
import prefuse.visual.VisualItem;

/**
 * @author Bogdan Mihaila
 */
public class HighlightLineControl extends ControlAdapter {
  private String labelName = "label";
  private final String activity;
  private int numLines = 0;

  public HighlightLineControl () {
    this(null);
  }

  /**
   * Creates a new highlight control that runs the given activity
   * whenever highlight changes.
   *
   * @param activity the update Activity to run
   */
  public HighlightLineControl (String activity) {
    this.activity = activity;
  }

  /**
   * Get the field name to use for text labels.
   *
   * @return the data field for text labels
   */
  public String getTextField () {
    return labelName;
  }

  /**
   * Set the field name to use for text labels.
   *
   * @param textField the data field for text labels
   */
  public void setTextField (String textField) {
    labelName = textField;
  }

  /**
   * Returns the text to draw.
   *
   * @param item the item to represent as a <code>String</code>
   * @return a <code>String</code> to draw
   */
  protected String getText (VisualItem item) {
    String s = null;
    if (item.canGetString(labelName)) {
      return item.getString(labelName);
    }
    return s;
  }

  @Override public void itemMoved (VisualItem item, MouseEvent e) {
    if (item instanceof NodeItem) {
      handleItem(item, e);
    }
  }

  @Override public void itemEntered (VisualItem item, MouseEvent e) {
    if (item instanceof NodeItem) {
      numLines = countLines(getText(item));
      handleItem(item, e);
    }
  }

  @Override public void itemExited (VisualItem item, MouseEvent e) {
    if (item instanceof NodeItem) {
      numLines = 0;
      setLineHighlight(item, -1);
    }
  }

  private void handleItem (VisualItem item, MouseEvent e) {
    Display display = (Display) e.getComponent();
    Point2D clickTarget = display.getAbsoluteCoordinate(e.getPoint(), null);
    int line = getClickedLine(item.getBounds(), clickTarget, numLines);
    setLineHighlight(item, line);
  }

  private void setLineHighlight (VisualItem item, int line) {
    if (item.canSetInt(HighlightableLineLabelRenderer.$HighlightedLineKey)) {
      item.setInt(HighlightableLineLabelRenderer.$HighlightedLineKey, line);
      if (activity != null)
        item.getVisualization().run(activity);
    }
  }

  private static int countLines (String text) {
    int newlines = 0;
    int index;
    do {
      index = text.indexOf("\n");
      if (index != -1) {
        newlines = newlines + 1;
        text = text.substring(index + 1);
      }
    } while (text.length() > 0 && index != -1);
    if (text.lastIndexOf("\n") != text.length() - 1) // count the last line even if it has no newline
      newlines = newlines + 1;
    return newlines;
  }

  private static int getClickedLine (Rectangle2D rectangle, Point2D clickTarget, int numLines) {
    double lineHeight = rectangle.getHeight() / numLines;
    int line = (int) Math.floor((clickTarget.getY() - rectangle.getY()) / lineHeight);
    return line;
  }

}
